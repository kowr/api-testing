import requests


class PokeAPIShape:
    base_url = "https://pokeapi.co/api/v2"
    pokemon_endpoint = "/pokemon-shape"

    def get_list_of_pokemon_shape(self):
        response = requests.get(self.base_url + self.pokemon_endpoint)
        assert response.status_code == 200
        return response

    def get_third_name_from_results(self):
        response = requests.get(self.base_url + self.pokemon_endpoint)
        body = response.json()
        return body["results"][2]["name"]

    def get_shape_of_pokemons(self, name=""):
        response = requests.get(f"{self.base_url}{self.pokemon_endpoint}/{name}")
        assert response.status_code == 200
        return response
