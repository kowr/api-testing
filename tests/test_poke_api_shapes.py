from src.pokeapi_shape_class import PokeAPIShape

pokeapi_shape = PokeAPIShape()


def test_count_of_pokemons():
    response = pokeapi_shape.get_list_of_pokemon_shape()
    body = response.json()
    assert body["count"] == len(body["results"])
    # assert body["count"] == 14


def test_third_name_from_results():
    body = pokeapi_shape.get_third_name_from_results()
    assert body == "fish"


def test_shape():
    body = pokeapi_shape.get_shape_of_pokemons().json()
    pokemon_name = body["results"][2]["name"]
    body = pokeapi_shape.get_shape_of_pokemons(pokemon_name).json()
    assert body["id"] == 3
