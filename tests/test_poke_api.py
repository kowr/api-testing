import requests


def test_response_is_not_empty():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    body = response.json()
    assert len(body["results"]) != 0


def test_check_response():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    assert response.status_code == 200


def test_is_overall_number_of_pokemons_equlas_1279():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    body = response.json()
    assert body["count"] == 1279


def test_is_server_responding_time_below_1s():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    time_of_response = response.elapsed.microseconds // 1000
    assert time_of_response < 1000


def test_check_is_size_of_respond_under_100kb():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    assert len(response.content) < 102400


def test_pagination():
    params = {
        "limit": 10,
        "offset": 21
    }
    response = requests.get("https://pokeapi.co/api/v2/pokemon", params=params)
    body = response.json()
    splitted = (body["results"][0]['url']).split("/")
    assert int(splitted[-2]) == (params["offset"] + 1)
    assert len(body["results"]) == 10
